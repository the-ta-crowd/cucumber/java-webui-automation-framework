# Java WebUI Automation Framework

This Java WebUI Automation project provides you with a framework that can be used for testing a webUI using Selenium WebDriver together with Cucumber. It will save you time on the initial setup of your test project. 

The framework is based on [SpringBoot](https://spring.io/projects/spring-boot) as core, with components for [Cucumber](https://cucumber.io/) and Selenium WebDriver. The Selenium drivers are provided by [WebdriverManager](https://github.com/bonigarcia/webdrivermanager). To make cleaner classes  without standard getters and setters, [Project Lombok](https://projectlombok.org/) is added. 

For accurate version numbers check the [pom.xml](pom.xml) properties section. 

## Table of Contents

- [Prerequisites](#prerequisites)
- [Setup Instructions](#setup)
- [How to run your tests](#running tests)
  - [In IntelliJ](#intellij)
  - [With Maven](#maven)
- [Test project architecture guidelines](documentation/TAGuidelines.md)
- [Licence](#licence)
 
## <a name="prerequisites"></a>Prerequisites
 
Before setting up your own test project, you need to clone or fork this framework over to a repository within your client's environment (from now on we will refer to this project as *framework*).
Additionally, you will need to have the following tools installed:
- IntelliJ IDEA (preferred)
- Java Development Kit 11 or higher
- Git
- Maven

## <a name="setup"></a>Setup Instructions
Take a look at our [demo project](https://gitlab.com/the-ta-crowd/cucumber/demo-project) for a complete example. For a quick set of instructions, see below.

Create a new Maven project for your integration tests (from now on we will refer to this project as *test project*).
Add the *framework* as a dependency to your *test project*'s `pom.xml`. 
  ```xml 
  <dependencies> 
      <dependency> 
          <groupId>com.tacrowd</groupId> 
          <artifactId>cucumber-framework</artifactId> 
          <version>${cucumber-framework.version}</version> 
      </dependency> 
  </dependencies> 
  ``` 
Add a properties section to your *test project's* `pom.xml` file, in which you specify the correct versions:
  ```xml
  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <java.version>11</java.version>
    <maven.compiler.source>${java.version}</maven.compiler.source>
    <maven.compiler.target>${java.version}</maven.compiler.target>
  
    <cucumber-framework.version>1.18.12</cucumber-framework.version>
  </properties>
  ```
Optionally, you can [change the default `com.tacrowd` packagename](documentation/SpringConfig.md)

Create the following packages within your `src/test/java/{package name}` directory   
  ![directory layout](/uploads/5546cad619fa2a276a114117d14da031/Image_4.jpg)

Add an application.yml file to `src/test/resources` to set the properties of your browsers, for example; 
  ```yml
  chrome: 
    headless: true 
    windowsize: "1920,1080" 
    arguments: 
      - "disable-infobars" 
      - "--disable-extensions" 
  firefox: 
    headless: false 
    windowsize: "1920,1080" 
    preferences: 
      dom.disable_beforeunload: true 
      network.dns.get-ttl: false 
      intl.accept_languages: nl 
  ``` 
The framework supports Chrome and Firefox out the box. You can also [add additional browser types](documentation/BrowserConfig.md) yourself.

Create an application-{environment}.yml file within the `src/test/recources folder`. Place a baseUrl property and value within this file. For example an "application-dev.yml" file containing:
```yml
baseUrl: http://localhost:8080
``` 

Add profiles for each environment and the build settings below to your *test project*'s `pom.xml`:
```xml
<profiles>
        <profile>
            <id>dev</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <activeProfile>dev</activeProfile>
            </properties>
        </profile>
        <profile>
            <id>prod</id>
            <properties>
                <activeProfile>prod</activeProfile>
            </properties>
        </profile>
    </profiles>

    <build>
        <resources>
            <resource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <configuration>
                        <argLine>-Dspring.profiles.active=${activeProfile}</argLine>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
```

Add a `Webhook.java`, elaborate instructions can be found [here](documentation/Webhook.md)

Create a BasePage class within the `pages` package. Autowire the *framework's* webdriver within this class:
```java
@Component
public class BasePage {

    @Autowired
    protected Webdriver driver;

}
```

Have each additional page object extend the BasePage. Don't forget to annotate each page object with the `@Component` annotation.

### Optional Steps
To create smarter test scenarios, you can add a DataHelper class to create just-in-time test data, see [DataTables](documentation/DataTables.md)

## <a name="running tests"></a>How to run your tests
If you want to run a small subset or single test, you can quickly do this directly from IntelliJ. To run all tests (e.g. in a pipeline), it is best to do so using Maven.
Your tests are located in your *test project* however, they have a dependency on the *framework*. So in order to run your tests you will first need to build the *framework*. 
This can be done by simply running `mvn clean install` on the *framework* project. 

### <a name="intellij"></a>In IntelliJ
Make sure you have the IntelliJ plugins **Cucumber for Java** and **Gherkin** installed. To run the tests, you need to set additional glue and VM options in the run configurations window. In the top bar choose `Run > Edit configurations > Templates > Cucumber Java`. 

For the glue, enter `{package name}.hooks` and `{package name}.steps`.

For the VM options, enter `-Dspring.profiles.active={environment}`. Optionally, you can use a different browser using `-DbrowserType=firefox` for example. 

This will be the default configuration for every test run. If you want to be able to change configurations regularly, select ‘Show this page’ as well. This will show the configurations window on every test run.

<img src="/uploads/5d8087a237eeb1cddac4b994aba19e77/MicrosoftTeams-image.png" width="600">

### <a name="maven"></a>With Maven
Maven automatically detects test runners named Test*.java or *Test.java. For more information see [maven surefire plugin](http://maven.apache.org/surefire/maven-surefire-plugin/examples/inclusion-exclusion.html). 

Create a `TestRunner.java` file in `src/test/java/{package name}` where the settings for the Cucumber run are stored. An example: 
```java
@RunWith(Cucumber.class) 
@CucumberOptions( 
        features = {"src/test/java/com/tacrowd/features"}, 
        glue = {"com.tacrowd.steps", "com.tacrowd.hooks"}, 
        plugin = {"pretty", "html:target/cucumber-html", "json:target/report.json"}, 
        strict = true) 
public class TestRunner { 

} 
``` 

More information on the different options for Cucumber in the TestRunner can be found at the [Cucumber Docs (under JUnit)](https://cucumber.io/docs/cucumber/api/#running-cucumber) 

Run the command `mvn test` to run all tests.  
This will show the results in a HTML file located at `{your project directory}/target/cucumber-html/` 
 
To run just specific tags via maven see [cucumber docs](https://cucumber.io/docs/cucumber/api/#tags)

## <a name="licence"></a>Licence

This Java WebUI Automation Framework is released under the [MIT licence](LICENCE)
