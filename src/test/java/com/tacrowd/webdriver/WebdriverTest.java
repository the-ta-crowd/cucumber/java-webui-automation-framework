package com.tacrowd.webdriver;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.springframework.test.util.ReflectionTestUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class WebdriverTest {

    @Mock
    private ChromeConfig chromeConfigMock;
    @Mock
    private FirefoxConfig firefoxConfigMock;

    @InjectMocks
    private Webdriver webdriver;

    @BeforeEach
    void setUp() {
        webdriver = new Webdriver();
        MockitoAnnotations.initMocks(this);
        when(chromeConfigMock.getOptions()).thenReturn(new ChromeOptions());
        when(firefoxConfigMock.getOptions()).thenReturn(new FirefoxOptions());
    }

    @Test
    void initChromeWebdriver() {
        // Given the browserType property is set to "chrome"
        ReflectionTestUtils.setField(webdriver, "browserType", "chrome");

        // When the webdriver is initialized
        webdriver.initWebdriver();

        // Then a chromedriver instance is created
        Object driver = ReflectionTestUtils.getField(webdriver, "webDriver");
        assertNotNull(driver);
        String driverInfo = ReflectionTestUtils.getField(driver, "driver").toString();
        assertThat(driverInfo, CoreMatchers.containsString("ChromeDriver"));
        webdriver.close();
    }

    @Test
    void initFirefoxWebdriver() {
        // Given the browserType property is set to "firefox"
        ReflectionTestUtils.setField(webdriver, "browserType", "firefox");

        // When the webdriver is initialized
        webdriver.initWebdriver();

        // Then a chromedriver instance is created
        Object driver = ReflectionTestUtils.getField(webdriver, "webDriver");
        assertNotNull(driver);
        String driverInfo = ReflectionTestUtils.getField(driver, "driver").toString();
        assertThat(driverInfo, CoreMatchers.containsString("FirefoxDriver"));
        webdriver.close();
    }

    @Test
    void initUnsupportedWebdriver() {
        // Given the browserType property is set to an unsupported browser type
        ReflectionTestUtils.setField(webdriver, "browserType", "random");

        // When the webdriver is initialized
        // Then an UnsupportedOperationException is thrown
        assertThrows(UnsupportedOperationException.class, () -> {
            webdriver.initWebdriver();
        });
    }
}