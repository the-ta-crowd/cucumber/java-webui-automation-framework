package com.tacrowd;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * The SpringConfiguration class facilitates necessary Spring Boot configuration.
 *
 * The {@link org.springframework.context.annotation.ComponentScan} allows you to autowire attributes within the
 * given package. The {@link org.springframework.boot.autoconfigure.EnableAutoConfiguration} allows for properties
 * to be configured from an application.yml file.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("com.tacrowd")
public class SpringConfiguration {
}