package com.tacrowd.webdriver;

import lombok.Data;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The ChromeConfig class allows you to configure additional Chrome options to your chromedriver instance, such as
 * the window size and whether or not to disable extensions.
 *
 * The {@link org.springframework.boot.context.properties.ConfigurationProperties} annotation allows you to initialize
 * the headless and arguments attributes at runtime from an application.yml file within the classpath. To initialize the
 * attributes properly, you have to follow the yaml conventions for the corresponding data types.
 */
@Data
@Component
@ConfigurationProperties(prefix = "chrome")
public class ChromeConfig {

    private boolean headless;
    private List<String> arguments;

    /**
     * Bundles the classes' attributes into a ChromeOptions object.
     *
     * @return  The ChromeOptions to be added to your chromedriver instance.
     */
    public ChromeOptions getOptions() {
        final ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(headless);
        if (arguments != null) {
            for (String argument : arguments) {
                chromeOptions.addArguments(argument);
            }
        }
        return chromeOptions;
    }

}
