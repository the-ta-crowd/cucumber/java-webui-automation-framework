package com.tacrowd.webdriver;

import lombok.Data;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * The FirefoxConfig class allows you to configure additional Firefox options to your firefoxdriver instance, such as
 * the window size and whether or not to disable extensions.
 *
 * The {@link org.springframework.boot.context.properties.ConfigurationProperties} annotation allows you to initialize
 * the headless and preferences attributes at runtime from an application.yml file within the classpath. To initialize the
 * attributes properly, you have to follow the yaml conventions for the corresponding data types.
 */
@Data
@Component
@ConfigurationProperties(prefix = "firefox")
public class FirefoxConfig {

    private boolean headless;
    private Map<String, String> preferences;

    /**
     * Bundles the classes' attributes into a FirefoxOptions object.
     *
     * @return  The FirefoxOptions to be added to your firefoxdriver instance.
     */
    public FirefoxOptions getOptions() {
        final FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(headless);

        if (preferences != null) {
            FirefoxProfile customProfile = new FirefoxProfile();
            for (Map.Entry<String, String> entry : preferences.entrySet()) {
                customProfile.setPreference(entry.getKey(), entry.getValue());
            }
            firefoxOptions.setProfile(customProfile);
        }

        return firefoxOptions;
    }

}
