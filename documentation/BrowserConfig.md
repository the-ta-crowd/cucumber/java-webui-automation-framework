# Add additional browser types
To add additional browser types, you need to:  
1. add properties to the `application.yml` file 
2. add a configuration class for this browser type 
3. add the driver initialization to the `Webdriver` class 

## 1. Add properties to the `application.yml` file 
In your *test project* there should be an `application.yml` file under `src/test/resources`.

- Add a new root key to this file, for example: `edge: ` 
- Under this key, add keys for all the browser-specific options you want to add when creating the driver for this browser to run your tests with. For example: 
  ```yml 
  edge:
    pageLoadStrategy: normal 
    capabilities: 
      - ms:inPrivate: true 
  ```

## 2. Add a configuration class for this browser type 
- In the *framework*, add a new Config java file under `com.tacrowd.webdriver`. For example `EdgeConfig.java` 
- Annotate this class with the `@Data`, `@Component` and `@ConfigurationProperties` annotations. 
- Set a prefix value to the ConfigurationProperties annotation, this prefix must correspond with the root key in the application.yml file. 
  The resulting config java class can thus be for example: 
  ```java 
  @Data 
  @Component 
  @ConfigurationProperties(prefix = “edge”) 
  Public class EdgeConfig { 
  
  } 
  ``` 
- Add fields for each key in the `application.yml` file under the root key for this browser type. For example, a String property “pageLoadStrategy” or a map of “capabilities”.     
  ```java 
  private String pageLoadStrategy; 
  private Map<String,String> capabilities; 
  ``` 
- Add a `getOptions()` method. This method will use the values in the fields of the Config class and set the browser options in the driver. 
  In this method, create a {browsertype}Options object and set its properties, for example: 
  ```java 
  public EdgeOptions getOptions() { 
    final EdgeOptions edgeOptions = new EdgeOptions(); 
    edgeOptions.setPageLoadStrategy(pageLoadStrategy); 
    if (capabilities != null) { 
        for (Map.Entry<String, String> capability: capabilities) { 
            edgeOptions.setCapability(entry.getKey(), entry.getValue); 
        } 
    } 
    return edgeOptions; 
  } 
  ```

## 3. Add the driver initialization to the `Webdriver` class 
In the *framework* there is a `Webdriver` class in the package `com.tacrowd.webdriver`.
- Add the Config class from **2.** as an autowired property to the Webdriver class. 
  ```java 
  @Autowired 
  private EdgeConfig edgeConfig; 
  ``` 
- In the `initWebdriver()` method, add a new case condition for your Webdriver. For instance: 
  ```java 
  case "edge": 
    WebDriverManager.getInstance(DriverManagerType.EDGE).setup(); 
    webDriver = new EventFiringWebDriver(new EdgeDriver(edgeConfig.getOptions())); 
    break; 
  ``` 