Test Automation Guidelines
======================
This document contains information and guidelines on how to use the test automation framework to setup your own project.

## Architecture
All the java code written for test automation can be found within `src/test/java`.

The project folder should contain the folders:

 - features
 - steps
 - pages
 - resources

As the content of the *project* folder suggests, the automated test has different levels: feature level, step level and page level. The *resources* folder contains the configuration files (subfolder *config*) and the objects/data types to help run the test. The following figure gives a summary of the different levels, where feature files are the highest level and pages are the lowest level.

![architecture](https://user-images.githubusercontent.com/15871496/38661816-68444784-3e32-11e8-80f3-48dc68bf1423.png)

### Features
The features folder contains `.feature` files per functionality. Each feature contains different scenarios to test this functionality. Thus each scenario is a test case. Scenarios consist of a scenario name and Gherkin. Each scenario should clarify the core of the test and be readable for everyone. So a scenario should have compact Gherkin. The following figure shows some guidelines for proper Gherkin.

![gherkin](https://user-images.githubusercontent.com/15871496/38661817-6867cdda-3e32-11e8-9247-6d1b12a723b7.png)

### Steps
Each line of Gherkin in a scenario corresponds with a function in a step file. This works with hooks from Selenium Cucumber. Within each function in a step file, functional steps can be found. These functional steps are all just calls to functions on the page level.

Each step file has a corresponding feature file. That means that if you want to create a new step file: first create a feature file, add a Scenario and let IntelliJ create a new corresponding step file for you. It is recommended to create a BaseSteps java file which can be extended by all your step files. In this BaseSteps file you can then add resources/pages which can be shared amongst the other step files. An example of some elements of a BaseSteps can be found in the coding guidelines below.

### Pages
Each functional step in the step files corresponds to a function on a page file. At the page level the elementary steps are taken such as "*click element x*", "*fill field y*" and "*wait for page to load*".

It is recommended to create a BasePage and adding the browser and base functions to it. Every page file that extends the BasePage can interact with the browser and access the base functions defined in BasePage. An example of some elements of a BasePage can be found in the coding guidelines below.

#### Coding guidelines
 - Selectors
 
 All elements on a page with which user interaction is needed must have selectors defined as such: *Name of element on screen* + *element type* + "Selector".
```java
private static final By ARRONDISEMENT_LIST_SELECTOR = By.cssSelector("select[name$=':'it9']");
private static final By DATE_FROM_INPUT_SELECTOR = By.cssSelector("input[name$=':id4']");
private static final By SHOW_BUTTON_SELECTOR = By.cssSelector("[id$=':buttonTonen']");
private static final By RESULT_START_TIME_SELECTOR = By.cssSelector("[id$=':c3']");
```
- BasePage

The code example below gives an idea of how one might setup a BasePage. This is just a short example, but you can add as many functions as you want to the BasePage which can be used by all pages which extend it.
```java
@Component
public class BasePage{

	//By making an object protected it is now accessible by all pages that extend the BasePage
	@Autowired
	protected Webdriver driver;
	
	private static final By USERMENU_DROPDOWN_SELECTOR = By.cssSelector("[id$=':usermenu']");
	private static final By LOGOUT_BUTTON_SELECTOR = By.cssSelector("[id$=':pt_cmi1']");

	/**
 	 * The logout function is available at all times
	 */
	public void logout(){
    	browser.findElement(USERMENU_DROPDOWN_SELECTOR).click();
	    browser.waitForElement(LOGOUT_BUTTON_SELECTOR).click();
	}
}
```

- BaseSteps

To make all page objects easily accessible from each steps file, you can create a BaseSteps class and autowire it with a Pages object:

```java
public class BaseSteps {

    @Autowired
    protected Pages pages;

}
```

The Pages class contains all the page objects in your project.

```java
@Component
public class Pages {

    @Autowired
    public CommonPage common;
    @Autowired
    public HomePage home;
    @Autowired
    public ResultsPage results;
}
```

Accessing a page object can then be done by having each steps file extend the BaseSteps and calling the pages property, for example:

```java
public class ResultsSteps extends BaseSteps {

    @Then("the user sees search results")
    public void theUserSeesSearchResults() {
        pages.results.verifySearchResultsPresent();
    }
}
```