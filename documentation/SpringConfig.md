This project assumes that you use the Maven Standard Directory Layout. Within this layout, the framework uses the main package `com.tacrowd`. For your test project to work, it will also have to use the main package `com.tacrowd`. It is possible to deviate from `com.tacrowd` as main package. Make sure to change the package names in both the test framework and your test project and to update the `SpringConfiguration.java` class in the framework: 
```java 
@ComponentScan("your.own.package") 
public class SpringConfiguration { 
``` 
The `@ComponentScan` in the `SpringConfiguration` class informs Spring in which packages it has to look for the Classes to be loaded using Spring. 